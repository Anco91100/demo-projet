<?php

namespace App\Controller;

use App\Entity\Adresse;
use App\Entity\Citoyen;
use App\Form\AdresseType;
use App\Form\CitoyenType;
use App\Form\FormAdresseType;
use App\Form\FormCitoyenType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/inscription", name="inscription_user")
     */
    public function inscription(Request $request, UserPasswordEncoderInterface $encoder, TokenGeneratorInterface $tokenGenerator, \Swift_Mailer $mailer) // Retourne la page d'inscription avec les formulaires ou redirige vers la page de connexion
    {
        $citoyen = new Citoyen();
        $adresse = new Adresse();
        $formCitoyen = $this->createForm(CitoyenType::class, $citoyen);
        $formAdresse = $this->createForm(AdresseType::class, $adresse);
        $manager = $this->getDoctrine()->getManager();
        $formAdresse->handleRequest($request);
        $formCitoyen->handleRequest($request);
        if ($formCitoyen->isSubmitted() && $formCitoyen->isValid() && $formAdresse->isSubmitted() && $formAdresse->isValid()) {
            $manager->persist($adresse);
            $manager->flush();
            $hash =  $encoder->encodePassword($citoyen, $citoyen->getMdpcitoyen());
            $citoyen->setMdpcitoyen($hash);
            $citoyen->setIdadresse($adresse->getIdadresse());
            $citoyen->setClevalidationcitoyen($tokenGenerator->generateToken());
            $citoyen->setValidationcitoyen(false);
            $citoyen->setRoles(['ROLE_NEW']);

            $manager->persist($citoyen);
            $manager->flush();
            $token = $citoyen->getClevalidationcitoyen();
            $mail = $citoyen->getCourielcitoyen();
            $url = $this->generateUrl('confirmation_compte', ['token' => $token, 'mail' => $mail], UrlGeneratorInterface::ABSOLUTE_URL);
            $msg = $message = (new \Swift_Message('Mail de confirmation'))
                ->setFrom('symfonyproject91@gmail.com')
                ->setTo($mail)
                ->setBody(
                    "Cliquer sur le lien suivant pour confirmer votre compte : " . $url,
                    'text/html'
                );
            $mailer->send($msg);
            $this->addFlash('message', 'Email de confirmation envoyer');
            return $this->redirectToRoute('connexion_profil');
        }
        return $this->render('user/inscription.html.twig', ['formCitoyen' => $formCitoyen->createView(), 'formAdresse' => $formAdresse->createView()]);
    }


    /**
     * @Route("/confirmation_compte/{token}/{mail}",name="confirmation_compte")
     */
    public function confirmationCompte($token, $mail)
    {
        $manager = $this->getDoctrine()->getManager();
        $email = $manager->getRepository(Citoyen::class)->findOneBy(['courielcitoyen' => $mail]);
        $tokenExist = $email->getClevalidationcitoyen();
        if ($token === $tokenExist) {
            $email->setClevalidationcitoyen(null);
            $email->setValidationcitoyen(true);
            $email->setRoles(['ROLE_USER']);
            $manager->persist($email);
            $manager->flush();
            $this->addFlash('notice', 'Compte activé !');
            return $this->redirectToRoute('home');
        } else {
            return $this->createNotFoundException('Erreur l\'utilisateur n\'existe pas');
        }
    }


    /**
     * @Route("/mot-de-passe-oublier",name="mot-de-passe-oublier")
     */
    public function mdpOublier(Request $requete, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer,  TokenGeneratorInterface $tokenGenerator)
    {

        if ($requete->isMethod('POST')) {
            $email = $requete->get('email');
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(Citoyen::class)->findOneBy(['courielcitoyen' => $email]);


            if ($user === null) {
                $this->addFlash('danger', 'Cet email n\'est pas valide');
                return $this->render('user/mdpOublier.html.twig');
            }
            $token = $tokenGenerator->generateToken();

            try {
                $user->setClevalidationcitoyen($token);
                $em->flush();
            } catch (\Exception $e) {
                $this->addFlash('mdpWarning', $e->getMessage());
                return $this->render('user/mdpOublier.html.twig');
            }

            $url = $this->generateUrl('reset_mdp', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);

            $message = (new \Swift_Message('Mot de passe oublié'))
                ->setFrom('symfonyproject91@gmail.com')
                ->setTo($user->getCourielcitoyen())
                ->setBody(
                    "Cliquez sur le lien suivant pour ré-initiliaser votre mot de passe : " . $url,
                    'text/html'
                );

            $mailer->send($message);

            $this->addFlash('mdpMailEnvoyer', 'Mail envoyé');

            return $this->redirectToRoute('home');
        }

        return $this->render('user/mdpOublier.html.twig');
    }


    /**
     * @Route("/reset_mdp/{token}", name="reset_mdp")
     *
     */
    public function ResetPassword(Request $requete, $token, UserPasswordEncoderInterface $encoder)
    {

        if ($requete->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();

            $user = $em->getRepository(Citoyen::class)->findOneBy(['clevalidationcitoyen' => $token]);

            if ($user === null) {
                $this->addFlash('mdpUserIntrouvable', 'Impossible de mettre à jour le mot de passe');
                return $this->render('user/reset_mdp.html.twig', ['token' => $token]);
            }

            $user->setClevalidationcitoyen(null);
            $user->setMdpcitoyen($encoder->encodePassword($user, $requete->request->get('password')));
            $em->flush();

            $this->addFlash('mdpMiseAJour', 'Mot de passe mis à jour');

            return $this->redirectToRoute('home');
        } else {

            return $this->render('user/reset_mdp.html.twig', ['token' => $token]);
        }
    }

    /**
     * @Route("/connexion",name="connexion_profil")
     */
    public function connexion() // Retourne la page de connexion
    {
        return $this->render('user/connexion.html.twig');
    }

    /**
     * @Route("/deconnexion",name="deconnexion_profil")
     */
    public function deconnexion()
    {
    }
}
