<?php

namespace App\Repository;

use App\Entity\CitoyenAdministreGroupe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CitoyenAdministreGroupe|null find($id, $lockMode = null, $lockVersion = null)
 * @method CitoyenAdministreGroupe|null findOneBy(array $criteria, array $orderBy = null)
 * @method CitoyenAdministreGroupe[]    findAll()
 * @method CitoyenAdministreGroupe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CitoyenAdministreGroupeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CitoyenAdministreGroupe::class);
    }

    // /**
    //  * @return CitoyenAdministreGroupe[] Returns an array of CitoyenAdministreGroupe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CitoyenAdministreGroupe
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
