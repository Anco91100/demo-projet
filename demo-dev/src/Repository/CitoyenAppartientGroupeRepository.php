<?php

namespace App\Repository;

use App\Entity\CitoyenAppartientGroupe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CitoyenAppartientGroupe|null find($id, $lockMode = null, $lockVersion = null)
 * @method CitoyenAppartientGroupe|null findOneBy(array $criteria, array $orderBy = null)
 * @method CitoyenAppartientGroupe[]    findAll()
 * @method CitoyenAppartientGroupe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CitoyenAppartientGroupeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CitoyenAppartientGroupe::class);
    }

    // /**
    //  * @return CitoyenAppartientGroupe[] Returns an array of CitoyenAppartientGroupe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CitoyenAppartientGroupe
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
