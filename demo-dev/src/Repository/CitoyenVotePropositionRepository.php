<?php

namespace App\Repository;

use App\Entity\CitoyenVoteProposition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CitoyenVoteProposition|null find($id, $lockMode = null, $lockVersion = null)
 * @method CitoyenVoteProposition|null findOneBy(array $criteria, array $orderBy = null)
 * @method CitoyenVoteProposition[]    findAll()
 * @method CitoyenVoteProposition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CitoyenVotePropositionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CitoyenVoteProposition::class);
    }

    // /**
    //  * @return CitoyenVoteProposition[] Returns an array of CitoyenVoteProposition objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CitoyenVoteProposition
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
