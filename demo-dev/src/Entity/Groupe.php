<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Groupe
 *
 * @ORM\Table(name="groupe")
 * @ORM\Entity(repositoryClass="App\Repository\GroupeRepository")
 */
class Groupe
{
    /**
     * @var int
     *
     * @ORM\Column(name="IDGROUPE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idgroupe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LIBELGROUPE", type="string", length=4096, nullable=true, options={"default"="NULL"})
     */
    private $libelgroupe = 'NULL';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="GROUPEPRIVE", type="boolean", nullable=true, options={"default"="NULL"})
     */
    private $groupeprive = 'NULL';

    public function getIdgroupe(): ?int
    {
        return $this->idgroupe;
    }

    public function getLibelgroupe(): ?string
    {
        return $this->libelgroupe;
    }

    public function setLibelgroupe(?string $libelgroupe): self
    {
        $this->libelgroupe = $libelgroupe;

        return $this;
    }

    public function getGroupeprive(): ?bool
    {
        return $this->groupeprive;
    }

    public function setGroupeprive(?bool $groupeprive): self
    {
        $this->groupeprive = $groupeprive;

        return $this;
    }
}
