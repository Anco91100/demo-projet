<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CitoyenVoteProposition
 *
 * @ORM\Table(name="citoyen_vote_proposition", indexes={@ORM\Index(name="IDCITOYEN", columns={"IDCITOYEN"})})
 * @ORM\Entity(repositoryClass="App\Repository\CitoyenVotePropositionRepository")
 */
class CitoyenVoteProposition
{
    /**
     * @var int
     *
     * @ORM\Column(name="IDPROPOSITION", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idproposition;

    /**
     * @var int
     *
     * @ORM\Column(name="IDCITOYEN", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idcitoyen;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="VOTE", type="boolean", nullable=true, options={"default"="NULL"})
     */
    private $vote = 'NULL';

    public function getIdproposition(): ?int
    {
        return $this->idproposition;
    }

    public function getIdcitoyen(): ?int
    {
        return $this->idcitoyen;
    }

    public function getVote(): ?bool
    {
        return $this->vote;
    }

    public function setVote(?bool $vote): self
    {
        $this->vote = $vote;

        return $this;
    }
}
