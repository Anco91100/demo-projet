<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CitoyenProposeProposition
 *
 * @ORM\Table(name="citoyen_propose_proposition", indexes={@ORM\Index(name="IDCITOYEN", columns={"IDCITOYEN"})})
 * @ORM\Entity(repositoryClass="App\Repository\CitoyenProposePropositionRepository")
 */
class CitoyenProposeProposition
{
    /**
     * @var int
     *
     * @ORM\Column(name="IDPROPOSITION", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idproposition;

    /**
     * @var int
     *
     * @ORM\Column(name="IDCITOYEN", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idcitoyen;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DATEPROPOSE", type="date", nullable=true, options={"default"="NULL"})
     */
    private $datepropose = 'NULL';

    public function getIdproposition(): ?int
    {
        return $this->idproposition;
    }

    public function getIdcitoyen(): ?int
    {
        return $this->idcitoyen;
    }

    public function getDatepropose(): ?\DateTimeInterface
    {
        return $this->datepropose;
    }

    public function setDatepropose(?\DateTimeInterface $datepropose): self
    {
        $this->datepropose = $datepropose;

        return $this;
    }
}
