<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Adresse
 *
 * @ORM\Table(name="adresse")
 * @ORM\Entity(repositoryClass="App\Repository\AdresseRepository")
 */
class Adresse
{
    /**
     * @var int
     *
     * @ORM\Column(name="IDADRESSE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idadresse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NUMRUE", type="string", length=16, nullable=true)
     */
    private $numrue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NOMRUE", type="string", length=1024, nullable=true)
     */
    private $nomrue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="VILLE", type="string", length=1024, nullable=true)
     */
    private $ville;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CP", type="string", length=128, nullable=true)
     */
    private $cp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="QUARTIER", type="string", length=128, nullable=true)
     */
    private $quartier;

    public function getIdadresse(): ?int
    {
        return $this->idadresse;
    }

    public function getNumrue(): ?string
    {
        return $this->numrue;
    }

    public function setNumrue(?string $numrue): self
    {
        $this->numrue = $numrue;

        return $this;
    }

    public function getNomrue(): ?string
    {
        return $this->nomrue;
    }

    public function setNomrue(?string $nomrue): self
    {
        $this->nomrue = $nomrue;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getQuartier(): ?string
    {
        return $this->quartier;
    }

    public function setQuartier(?string $quartier): self
    {
        $this->quartier = $quartier;

        return $this;
    }
}
