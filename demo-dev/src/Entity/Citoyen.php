<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Citoyen
 *
 * @ORM\Table(name="citoyen", indexes={@ORM\Index(name="IDADRESSE", columns={"IDADRESSE"})})
 * @ORM\Entity(repositoryClass="App\Repository\CitoyenRepository")
 */
class Citoyen implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="IDCITOYEN", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcitoyen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NOMCITOYEN", type="string", length=256, nullable=true)
     */
    private $nomcitoyen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PRENOMCITOYEN", type="string", length=256, nullable=true)
     */
    private $prenomcitoyen;

    /**
     * @var string
     *
     * @ORM\Column(name="COURIELCITOYEN", type="string", length=512, nullable=false)
     */
    private $courielcitoyen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TELCITOYEN", type="string", length=256, nullable=true)
     */
    private $telcitoyen;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="NAISSANCECITOYEN", type="date", nullable=true)
     */
    private $naissancecitoyen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="MDPCITOYEN", type="string", length=256, nullable=true)
     */
    private $mdpcitoyen;

    public $confirm_mdp;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="VALIDATIONCITOYEN", type="boolean", nullable=true)
     */
    private $validationcitoyen;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CLEVALIDATIONCITOYEN", type="string", length=256, nullable=true)
     */
    private $clevalidationcitoyen;

    /**
     * @var int|null
     *
     * @ORM\Column(name="IDADRESSE", type="integer", nullable=true)
     */
    private $idadresse;

    public function getIdcitoyen(): ?int
    {
        return $this->idcitoyen;
    }

    public function getNomcitoyen(): ?string
    {
        return $this->nomcitoyen;
    }

    public function setNomcitoyen(?string $nomcitoyen): self
    {
        $this->nomcitoyen = $nomcitoyen;

        return $this;
    }

    public function getPrenomcitoyen(): ?string
    {
        return $this->prenomcitoyen;
    }

    public function setPrenomcitoyen(?string $prenomcitoyen): self
    {
        $this->prenomcitoyen = $prenomcitoyen;

        return $this;
    }

    public function getCourielcitoyen(): ?string
    {
        return $this->courielcitoyen;
    }

    public function setCourielcitoyen(string $courielcitoyen): self
    {
        $this->courielcitoyen = $courielcitoyen;

        return $this;
    }

    public function getTelcitoyen(): ?string
    {
        return $this->telcitoyen;
    }

    public function setTelcitoyen(?string $telcitoyen): self
    {
        $this->telcitoyen = $telcitoyen;

        return $this;
    }

    public function getNaissancecitoyen(): ?\DateTimeInterface
    {
        return $this->naissancecitoyen;
    }

    public function setNaissancecitoyen(?\DateTimeInterface $naissancecitoyen): self
    {
        $this->naissancecitoyen = $naissancecitoyen;

        return $this;
    }

    public function getMdpcitoyen(): ?string
    {
        return $this->mdpcitoyen;
    }

    public function setMdpcitoyen(?string $mdpcitoyen): self
    {
        $this->mdpcitoyen = $mdpcitoyen;

        return $this;
    }

    public function getValidationcitoyen(): ?bool
    {
        return $this->validationcitoyen;
    }

    public function setValidationcitoyen(?bool $validationcitoyen): self
    {
        $this->validationcitoyen = $validationcitoyen;

        return $this;
    }

    public function getClevalidationcitoyen(): ?string
    {
        return $this->clevalidationcitoyen;
    }

    public function setClevalidationcitoyen(?string $clevalidationcitoyen): self
    {
        $this->clevalidationcitoyen = $clevalidationcitoyen;

        return $this;
    }

    public function getIdadresse(): ?int
    {
        return $this->idadresse;
    }

    public function setIdadresse(?int $idadresse): self
    {
        $this->idadresse = $idadresse;

        return $this;
    }

    public function getSalt()
    {
        // leaving blank - I don't need/have a password!
    }


    public function setRoles(array $roles)
    {
        $this->roles = $roles;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function getPassword(): ?string
    {
        return $this->mdpcitoyen;
    }

    public function getUsername(): ?string
    {
        return $this->prenomcitoyen;
    }


    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}
