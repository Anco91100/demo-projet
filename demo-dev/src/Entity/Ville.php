<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ville
 *
 * @ORM\Table(name="ville")
 * @ORM\Entity(repositoryClass="App\Repository\VilleRepository")
 */
class Ville
{
    /**
     * @var int
     *
     * @ORM\Column(name="IDVILLE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idville;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CPVILLE", type="string", length=256, nullable=true, options={"default"="NULL"})
     */
    private $cpville = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="NOMVILLE", type="string", length=256, nullable=true, options={"default"="NULL"})
     */
    private $nomville = 'NULL';

    public function getIdville(): ?int
    {
        return $this->idville;
    }

    public function getCpville(): ?string
    {
        return $this->cpville;
    }

    public function setCpville(?string $cpville): self
    {
        $this->cpville = $cpville;

        return $this;
    }

    public function getNomville(): ?string
    {
        return $this->nomville;
    }

    public function setNomville(?string $nomville): self
    {
        $this->nomville = $nomville;

        return $this;
    }
}
