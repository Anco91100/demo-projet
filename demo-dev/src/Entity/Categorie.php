<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie", indexes={@ORM\Index(name="IDGROUPE", columns={"IDGROUPE"})})
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="IDGROUPE", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $idgroupe = 'NULL';

    /**
     * @var int
     *
     * @ORM\Column(name="IDCATEGORIE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcategorie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LIBELCATEGORIE", type="string", length=1024, nullable=true, options={"default"="NULL"})
     */
    private $libelcategorie = 'NULL';

    public function getIdgroupe(): ?int
    {
        return $this->idgroupe;
    }

    public function setIdgroupe(?int $idgroupe): self
    {
        $this->idgroupe = $idgroupe;

        return $this;
    }

    public function getIdcategorie(): ?int
    {
        return $this->idcategorie;
    }

    public function getLibelcategorie(): ?string
    {
        return $this->libelcategorie;
    }

    public function setLibelcategorie(?string $libelcategorie): self
    {
        $this->libelcategorie = $libelcategorie;

        return $this;
    }
}
