<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proposition
 *
 * @ORM\Table(name="proposition", indexes={@ORM\Index(name="IDGROUPE", columns={"IDGROUPE"}), @ORM\Index(name="IDGROUPE_1", columns={"IDGROUPE_1", "IDCATEGORIE"})})
 * @ORM\Entity(repositoryClass="App\Repository\PropositionRepository")
 */
class Proposition
{
    /**
     * @var int
     *
     * @ORM\Column(name="IDPROPOSITION", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idproposition;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TITREPROPOSITION", type="string", length=1024, nullable=true, options={"default"="NULL"})
     */
    private $titreproposition = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="DETAILPROPOSITION", type="string", length=4296, nullable=true, options={"default"="NULL"})
     */
    private $detailproposition = 'NULL';

    /**
     * @var int
     *
     * @ORM\Column(name="IDGROUPE", type="integer", nullable=false)
     */
    private $idgroupe;

    /**
     * @var int
     *
     * @ORM\Column(name="IDGROUPE_1", type="integer", nullable=false)
     */
    private $idgroupe1;

    /**
     * @var int
     *
     * @ORM\Column(name="IDCATEGORIE", type="integer", nullable=false)
     */
    private $idcategorie;

    public function getIdproposition(): ?int
    {
        return $this->idproposition;
    }

    public function getTitreproposition(): ?string
    {
        return $this->titreproposition;
    }

    public function setTitreproposition(?string $titreproposition): self
    {
        $this->titreproposition = $titreproposition;

        return $this;
    }

    public function getDetailproposition(): ?string
    {
        return $this->detailproposition;
    }

    public function setDetailproposition(?string $detailproposition): self
    {
        $this->detailproposition = $detailproposition;

        return $this;
    }

    public function getIdgroupe(): ?int
    {
        return $this->idgroupe;
    }

    public function setIdgroupe(int $idgroupe): self
    {
        $this->idgroupe = $idgroupe;

        return $this;
    }

    public function getIdgroupe1(): ?int
    {
        return $this->idgroupe1;
    }

    public function setIdgroupe1(int $idgroupe1): self
    {
        $this->idgroupe1 = $idgroupe1;

        return $this;
    }

    public function getIdcategorie(): ?int
    {
        return $this->idcategorie;
    }

    public function setIdcategorie(int $idcategorie): self
    {
        $this->idcategorie = $idcategorie;

        return $this;
    }
}
