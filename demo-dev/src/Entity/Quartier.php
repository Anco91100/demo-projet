<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Quartier
 *
 * @ORM\Table(name="quartier", indexes={@ORM\Index(name="IDVILLE", columns={"IDVILLE"})})
 * @ORM\Entity(repositoryClass="App\Repository\QuartierRepository")
 */
class Quartier
{
    /**
     * @var int
     *
     * @ORM\Column(name="IDQUARTIER", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idquartier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NOMQUARTIER", type="string", length=1024, nullable=true, options={"default"="NULL"})
     */
    private $nomquartier = 'NULL';

    /**
     * @var int
     *
     * @ORM\Column(name="IDVILLE", type="integer", nullable=false)
     */
    private $idville;

    public function getIdquartier(): ?int
    {
        return $this->idquartier;
    }

    public function getNomquartier(): ?string
    {
        return $this->nomquartier;
    }

    public function setNomquartier(?string $nomquartier): self
    {
        $this->nomquartier = $nomquartier;

        return $this;
    }

    public function getIdville(): ?int
    {
        return $this->idville;
    }

    public function setIdville(int $idville): self
    {
        $this->idville = $idville;

        return $this;
    }
}
