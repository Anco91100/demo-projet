<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CitoyenAppartientGroupe
 *
 * @ORM\Table(name="citoyen_appartient_groupe", indexes={@ORM\Index(name="IDCITOYEN", columns={"IDCITOYEN"})})
 * @ORM\Entity(repositoryClass="App\Repository\CitoyenAppartientGroupeRepository")
 */
class CitoyenAppartientGroupe
{
    /**
     * @var int
     *
     * @ORM\Column(name="IDGROUPE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idgroupe;

    /**
     * @var int
     *
     * @ORM\Column(name="IDCITOYEN", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idcitoyen;

    public function getIdgroupe(): ?int
    {
        return $this->idgroupe;
    }

    public function getIdcitoyen(): ?int
    {
        return $this->idcitoyen;
    }
}
