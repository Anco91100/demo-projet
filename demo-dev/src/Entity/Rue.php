<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rue
 *
 * @ORM\Table(name="rue", indexes={@ORM\Index(name="IDVILLE", columns={"IDVILLE"})})
 * @ORM\Entity(repositoryClass="App\Repository\RueRepository")
 */
class Rue
{
    /**
     * @var int
     *
     * @ORM\Column(name="IDRUE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LIBELRUE", type="string", length=4096, nullable=true, options={"default"="NULL"})
     */
    private $libelrue = 'NULL';

    /**
     * @var int
     *
     * @ORM\Column(name="IDVILLE", type="integer", nullable=false)
     */
    private $idville;

    public function getIdrue(): ?int
    {
        return $this->idrue;
    }

    public function getLibelrue(): ?string
    {
        return $this->libelrue;
    }

    public function setLibelrue(?string $libelrue): self
    {
        $this->libelrue = $libelrue;

        return $this;
    }

    public function getIdville(): ?int
    {
        return $this->idville;
    }

    public function setIdville(int $idville): self
    {
        $this->idville = $idville;

        return $this;
    }
}
